/**
 *
 * @author Tim Jabs
 */
public class WeekendCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 0) {
            //expect value at first parameter
            calculateWeekendDays(Integer.parseInt(args[0]));
        } else {
            //default
            calculateWeekendDays(1093);
        }
    }

    private static void calculateWeekendDays(int days) {
        int weekdays = 0;
        int stopper = 0;
        for (int i = 1; i <= days; i++) {
            if (stopper == 5) {
                weekdays = weekdays + 2;
                i++;
                stopper = 0;
            } else {
                stopper++;
            }
        }
        System.out.println("Wochendtage: " + weekdays);
    }
}
